## Contact API Assignment

### Assignment Step Description

In this Case study: ContactAPI, we will create a RESTful application.
Representational State Transfer (REST) is an architectural style that specifies constraints.
In the REST architectural style, data and functionality are considered resources and are accessed using Uniform Resource Identifiers (URIs), typically links on the Web.

Resources are manipulated using a fixed set of four create, read, update, delete operations: GET, POST, PUT and DELETE.

- POST creates a new resource.
- GET retrieves the current state of a resource in some representation.
- PUT transfers a new state onto a resource.
- DELETE deletes a resource.

### Problem Statement

In this case study, we will develop a RESTful application with which we will manage contacts information.

### Following are the broad tasks

```
Create a new Contact
Retreive all Contacts
Retreive Contact based on ContactId
Update Contact based on ContactId
Delete Contact based on ContactId
```

### API should have following endpoints

```
GET - /api/contact - Should retreive all contacts
GET - /api/contact/<contactid> - Should retreive contact based on contactId
POST - /api/contact - Should create new contact
PUT - /api/contact/<contactid> - Should update existing contact based on contactId
DELETE - /api/contact/<contactid> - Should delete existing contact based on contactId
```

### Steps to be followed

```
Step 1: Define the data model class - Contact.
Step 2: Define the entity in DataContext.
Step 3: Implement all methods in ContactRepository.
Step 4: Implement all methods in ContactService.
Step 5: Add code in controller methods to work with RESTful web services.
Step 6: Resolve DbContext options and all the dependencies in Startup.cs file
Step 6: Run all test cases to verify the correct functionality of Repository, Services & Controller.
```