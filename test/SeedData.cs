﻿using cgi_tollgate_assignment_contact_api.DAL;
using cgi_tollgate_assignment_contact_api.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace test
{
    public static class SeedData
    {
        public static void PopulateTestData(ContactDataContext context)
        {
            AddContacts(context);
        }

        private static void AddContacts(ContactDataContext context)
        {
            context.Contacts.Add(new Contact { FirstName="Ram", LastName="Kumar", Email="Ram@gmail.com", Phone="247254878", City="New Delhi" });
            context.SaveChanges();
        }

        public static void DeleteTestData(ContactDataContext context)
        {
            context.Contacts.RemoveRange(context.Contacts.ToList());
        }
    }

}
