﻿using cgi_tollgate_assignment_contact_api.DAL;
using cgi_tollgate_assignment_contact_api.Exceptions;
using cgi_tollgate_assignment_contact_api.Models;
using cgi_tollgate_assignment_contact_api.Services;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace test.Service
{
    [TestCaseOrderer("test.PriorityOrderer", "test")]
    public class ContactServiceTest
    {
        #region Positive Tests
        [Fact, TestPriority(1)]
        public void AddContactShouldReturnContact()
        {
            var mockRepo = new Mock<IContactRepository>();
            Contact contact = new Contact { FirstName = "Vishal", LastName = "Singh", Email = "vishal@gmail.com", Phone = "287427878", City = "Bangalore" };
            mockRepo.Setup(repo => repo.AddContact(contact)).Returns(contact);
            var service = new ContactService(mockRepo.Object);
            var actual = service.AddContact(contact);
            Assert.IsAssignableFrom<Contact>(actual);
        }
        [Fact, TestPriority(2)]
        public void GetContactByIdShouldReturnContact()
        {
            int contactId = 1;
            Contact contact = new Contact { FirstName = "Peter", LastName = "Parker", Email = "Peter@gmail.com", Phone = "27842749", City = "Chennai" };
            var mockRepo = new Mock<IContactRepository>();
            mockRepo.Setup(repo => repo.GetContactById(contactId)).Returns(contact);
            var service = new ContactService(mockRepo.Object);
            var actual = service.GetContactById(contactId);
            Assert.NotNull(actual);
            Assert.IsAssignableFrom<Contact>(actual);
            Assert.Equal("Peter", actual.FirstName);
        }

        [Fact, TestPriority(3)]
        public void GetContactsShouldReturnListOfContacts()
        {
            var mockRepo = new Mock<IContactRepository>();
            mockRepo.Setup(repo => repo.GetContacts()).Returns(this.GetContacts());
            var service = new ContactService(mockRepo.Object);
            var actual = service.GetContacts();
            Assert.IsAssignableFrom<List<Contact>>(actual);
        }

        [Fact, TestPriority(4)]
        public void UpdateContactShouldReturnTrue()
        {
            int Id = 1;
            Contact contact = new Contact { FirstName = "Peter", LastName = "Parker", Email = "Peter@hotmail.com", Phone = "9812345678", City = "Hyderabad" };
            var mockRepo = new Mock<IContactRepository>();
            mockRepo.Setup(repo => repo.GetContactById(Id)).Returns(contact);
            mockRepo.Setup(repo => repo.UpdateContact(Id, contact)).Returns(true);
            var service = new ContactService(mockRepo.Object);
            var actual = service.UpdateContact(Id, contact);
            Assert.True(actual);
        }

        [Fact, TestPriority(5)]
        public void DeleteContactShouldReturnTrue()
        {
            var mockRepo = new Mock<IContactRepository>();
            var Id = 2;
            Contact contact = new Contact { };
            mockRepo.Setup(repo => repo.GetContactById(Id)).Returns(contact);
            mockRepo.Setup(repo => repo.DeleteContact(Id)).Returns(true);
            var service = new ContactService(mockRepo.Object);
            var actual = service.DeleteContact(Id);
            Assert.True(actual);
        }

        private List<Contact> GetContacts()
        {
            List<Contact> categories = new List<Contact> {
               new Contact {FirstName="Steve", LastName="Smith", Email="steve@gmail.com", Phone="2746289649", City="New Delhi" }
            };
            return categories;
        }
        #endregion
        #region Negative Tests
        [Fact, TestPriority(6)]
        public void GetContactByIdShouldThrowException()
        {
            int id = 5;
            Contact contact = null;
            var mockRepo = new Mock<IContactRepository>();
            mockRepo.Setup(repo => repo.GetContactById(id)).Returns(contact);
            var service = new ContactService(mockRepo.Object);
            var actual = Assert.Throws<ContactNotFoundException>(() => service.GetContactById(id));
            Assert.Equal($"Contact with id: {id} does not exists", actual.Message);
        }

        [Fact, TestPriority(7)]
        public void DeleteContactShouldThrowException()
        {
            var mockRepo = new Mock<IContactRepository>();
            var contactId = 5;
            Contact contact = null;
            mockRepo.Setup(repo => repo.GetContactById(contactId)).Returns(contact);
            mockRepo.Setup(repo => repo.DeleteContact(contactId)).Returns(false);
            var service = new ContactService(mockRepo.Object);
            var actual = Assert.Throws<ContactNotFoundException>(() => service.DeleteContact(contactId));
            Assert.Equal($"Contact with id: {contactId} does not exists", actual.Message);
        }

        [Fact, TestPriority(8)]
        public void UpdateContactShouldThrowException()
        {
            int contactId = 5;
            Contact contact = new Contact { FirstName = "Peter", LastName = "Parker", Email = "Peter@gmail.com", Phone = "27842749", City = "Chennai" };
            Contact _contact = null;
            var mockRepo = new Mock<IContactRepository>();
            mockRepo.Setup(repo => repo.GetContactById(contactId)).Returns(_contact);
            mockRepo.Setup(repo => repo.UpdateContact(contactId, contact)).Returns(false);
            var service = new ContactService(mockRepo.Object);
            var actual = Assert.Throws<ContactNotFoundException>(() => service.UpdateContact(contactId, contact));
            Assert.Equal($"Contact with id: {contactId} does not exists", actual.Message);
        }
        #endregion
    }
}
