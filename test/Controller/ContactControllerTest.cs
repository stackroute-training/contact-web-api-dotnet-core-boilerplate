﻿using cgi_tollgate_assignment_contact_api;
using cgi_tollgate_assignment_contact_api.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace test.Controller
{
    [Collection("Db collection")]
    [TestCaseOrderer("test.PriorityOrderer", "test")]
    public class ContactControllerTest
    {
        private readonly HttpClient _client;
        public ContactControllerTest(CustomWebApplicationFactory<Startup> factory)
        {
            _client = factory.CreateClient();
        }

        #region Positive Tests
        [Fact, TestPriority(1)]
        public async Task AddContactShouldSuccess()
        {
            Contact contact = new Contact { FirstName = "Amit", LastName = "Kumar", Email = "Amit@gmail.com", Phone = "2874527428", City = "Bangaluru" };
            HttpRequestMessage request = new HttpRequestMessage();
            MediaTypeFormatter formatter = new JsonMediaTypeFormatter();
            // The endpoint or route of the controller action.
            var httpResponse = await _client.PostAsync<Contact>("/api/contact", contact, formatter);
            // Deserialize and examine results.
            var stringResponse = await httpResponse.Content.ReadAsStringAsync();
            var response = JsonConvert.DeserializeObject<Contact>(stringResponse);
            Assert.Equal(HttpStatusCode.Created, httpResponse.StatusCode);
            Assert.IsAssignableFrom<Contact>(response);
        }

        [Fact, TestPriority(2)]
        public async Task GetContactByIdShouldSuccess()
        {
            // The endpoint or route of the controller action.
            int contactId = 1;
            var httpResponse = await _client.GetAsync($"/api/contact/{contactId}");
            // Must be successful.
            httpResponse.EnsureSuccessStatusCode();
            // Deserialize and examine results.
            var stringResponse = await httpResponse.Content.ReadAsStringAsync();
            var contact = JsonConvert.DeserializeObject<Contact>(stringResponse);
            Assert.Equal("Ram", contact.FirstName);
        }

        [Fact, TestPriority(3)]
        public async Task GetAllContactsShouldSuccess()
        {
            // The endpoint or route of the controller action.
            var httpResponse = await _client.GetAsync("/api/contact");
            // Must be successful.
            httpResponse.EnsureSuccessStatusCode();
            // Deserialize and examine results.
            var stringResponse = await httpResponse.Content.ReadAsStringAsync();
            var contacts = JsonConvert.DeserializeObject<IEnumerable<Contact>>(stringResponse);
            Assert.Contains(contacts, c => c.FirstName == "Amit");
        }

        [Fact, TestPriority(4)]
        public async Task UpdateContactShouldSuccess()
        {
            int contactId = 1;
            Contact contact = new Contact { FirstName = "Amit", LastName = "Kumar", Email = "Amit@hotmail.com", Phone = "9812345678", City = "Mumbai" };
            HttpRequestMessage request = new HttpRequestMessage();
            MediaTypeFormatter formatter = new JsonMediaTypeFormatter();
            // The endpoint or route of the controller action.
            var httpResponse = await _client.PutAsync<Contact>($"/api/contact/{contactId}", contact, formatter);
            // Deserialize and examine results.
            var stringResponse = await httpResponse.Content.ReadAsStringAsync();
            Assert.Equal(HttpStatusCode.OK, httpResponse.StatusCode);
            Assert.True(Convert.ToBoolean(stringResponse));
        }

        [Fact, TestPriority(5)]
        public async Task DeleteContactShouldSuccess()
        {
            int contactId = 2;
            // The endpoint or route of the controller action.
            var httpResponse = await _client.DeleteAsync($"/api/contact/{contactId}");
            // Deserialize and examine results.
            var stringResponse = await httpResponse.Content.ReadAsStringAsync();
            Assert.Equal(HttpStatusCode.OK, httpResponse.StatusCode);
            Assert.True(Convert.ToBoolean(stringResponse));
        }
        #endregion
        #region Negative Tests
        [Fact, TestPriority(6)]
        public async Task GetContactByIdShouldReturnNotFound()
        {
            // The endpoint or route of the controller action.
            int contactId = 3;
            var httpResponse = await _client.GetAsync($"/api/contact/{contactId}");
            // Deserialize and examine results.
            var stringResponse = await httpResponse.Content.ReadAsStringAsync();
            Assert.Equal(HttpStatusCode.NotFound, httpResponse.StatusCode);
            Assert.Equal($"Contact with id: {contactId} does not exists", stringResponse);
        }

        [Fact, TestPriority(7)]
        public async Task UpdateContactShouldFail()
        {
            int contactId = 3;
            Contact contact = new Contact { FirstName = "Amit", LastName = "Kumar", Email = "Amit@hotmail.com", Phone = "9812345678", City = "Mumbai" };
            HttpRequestMessage request = new HttpRequestMessage();
            MediaTypeFormatter formatter = new JsonMediaTypeFormatter();
            // The endpoint or route of the controller action.
            var httpResponse = await _client.PutAsync<Contact>($"/api/contact/{contactId}", contact, formatter);
            // Deserialize and examine results.
            var stringResponse = await httpResponse.Content.ReadAsStringAsync();
            Assert.Equal(HttpStatusCode.NotFound, httpResponse.StatusCode);
            Assert.Equal($"Contact with id: {contactId} does not exists", stringResponse);
        }

        [Fact, TestPriority(8)]
        public async Task DeleteContactShouldFail()
        {
            int contactId = 3;
            // The endpoint or route of the controller action.
            var httpResponse = await _client.DeleteAsync($"/api/contact/{contactId}");
            // Deserialize and examine results.
            var stringResponse = await httpResponse.Content.ReadAsStringAsync();
            Assert.Equal(HttpStatusCode.NotFound, httpResponse.StatusCode);
            Assert.Equal($"Contact with id: {contactId} does not exists", stringResponse);
        }
        #endregion
    }
}
