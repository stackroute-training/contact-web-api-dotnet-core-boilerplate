using cgi_tollgate_assignment_contact_api.DAL;
using cgi_tollgate_assignment_contact_api.Models;
using System;
using System.Collections.Generic;
using Xunit;

namespace test.Repository
{
    [Collection("Database collection")]
    [TestCaseOrderer("test.PriorityOrderer", "test")]
    public class ContactRepositoryTest
    {
        ContactRepository repository;
        public ContactRepositoryTest(DatabaseFixture databaseFixture)
        {
            repository = new ContactRepository(databaseFixture.context);
        }

        [Fact, TestPriority(1)]
        public void GetAllContactsShouldReturnList()
        {
            var actual = repository.GetContacts();
            Assert.IsAssignableFrom<IEnumerable<Contact>>(actual);
            Assert.NotNull(actual);
        }

        [Fact, TestPriority(2)]
        public void GetContactByIdShouldReturnContact()
        {
            int contactId = 1;

            var actual = repository.GetContactById(contactId);
            Assert.IsAssignableFrom<Contact>(actual);
            Assert.NotNull(actual);
        }

        [Fact, TestPriority(3)]
        public void UpdateContactShouldSuccess()
        {
            Contact contact = repository.GetContactById(1);
            contact.City = "Mumbai";

            var actual = repository.UpdateContact(1, contact);
            Assert.True(actual);
        }

        [Fact, TestPriority(4)]
        public void AddContactShouldSuccess()
        {
            Contact contact = new Contact { FirstName = "Amit", LastName = "Kumar", Email = "Amit@gmail.com", Phone = "2874527428", City = "Bangalore" };
            var actual = repository.AddContact(contact);

            Assert.IsAssignableFrom<Contact>(actual);
            Assert.Equal(2, actual.ContactId);
        }

        [Fact, TestPriority(5)]
        public void DeleteContactShouldSuccess()
        {
            int contactId = 2;

            var actual = repository.DeleteContact(contactId);
            Assert.True(actual);
            Assert.Null(repository.GetContactById(contactId));
        }
    }
}
