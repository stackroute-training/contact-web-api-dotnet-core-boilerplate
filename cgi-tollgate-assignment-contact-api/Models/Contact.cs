﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace cgi_tollgate_assignment_contact_api.Models
{
    public class Contact
    {
        /*
         * This model class should have six properties
         * ContactId - int (Should be a primary key and auto-generated).
         * FirstName - string
         * LastName - string
         * Email - string
         * Phone - string
         * City - string
         */
        
        public int ContactId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set;}
        public string Email { get; set; }
        public string Phone { get; set; }
        public string City { get; set; }
    }
}
