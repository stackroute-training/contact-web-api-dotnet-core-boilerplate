﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace cgi_tollgate_assignment_contact_api.Exceptions
{
    public class ContactNotFoundException:ApplicationException
    {
        // Write Your code here to make user defined exception class
        public ContactNotFoundException(int id) : base(String.Format($"Contact with id: {id} does not exists"))
        {

        }
    }
}
