﻿using cgi_tollgate_assignment_contact_api.DAL;
using cgi_tollgate_assignment_contact_api.Exceptions;
using cgi_tollgate_assignment_contact_api.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace cgi_tollgate_assignment_contact_api.Services
{
    public class ContactService : IContactService
    {
        /*
        Use constructor Injection to inject all required dependencies.
        */
        private readonly IContactRepository repo;
        public ContactService(IContactRepository _repo)
        {
            this.repo = _repo;
            
        }
        
        /*
	    * This method should be used to save a new contact.
	    */
        public Contact AddContact(Contact contact)
        {
            return repo.AddContact(contact);
        }

        /* This method should be used to delete an existing contact.
         * Throw ContactNotFoundException in case contact not found based on specified contactId. 
         */
        public bool DeleteContact(int id)
        {
            try
            {
                bool check = repo.DeleteContact(id);
                if (check)
                {
                    return true;
                }
                else
                {
                    throw new ContactNotFoundException(id);
                }
            }
            catch (ContactNotFoundException)
            {

                throw new ContactNotFoundException(id);
            }  
        }

        /* This method should be used to get a contact by contactId. 
         * Throw ContactNotFoundException in case contact not found based on specified contactId.
         */
        public Contact GetContactById(int id)
        {
            try
            {
                Contact contact = repo.GetContactById(id);
                if(contact == null)
                {
                    throw new ContactNotFoundException(id);
                }
                else
                {
                    return contact;
                }
            }
            catch (ContactNotFoundException)
            {

                throw new ContactNotFoundException(id);
            }
        }

        /* This method should be used to get a contact all contacts. */
        public List<Contact> GetContacts()
        {
            return repo.GetContacts();
        }

        /* This method should be used to update a contact by contactId.
         * Throw ContactNotFoundException in case contact not found based on specified contactId. 
         */
        public bool UpdateContact(int id, Contact contact)
        {
            try
            {
                bool check = repo.UpdateContact(id, contact);
                if (check == true)
                {
                    return true;
                }
                else
                {
                    throw new ContactNotFoundException(id);
                }

            }
            catch (ContactNotFoundException  )
            {
               
                throw new ContactNotFoundException(id);
                
            }
        }

        
    }
}
