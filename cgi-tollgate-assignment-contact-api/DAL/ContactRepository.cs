﻿using cgi_tollgate_assignment_contact_api.Exceptions;
using cgi_tollgate_assignment_contact_api.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace cgi_tollgate_assignment_contact_api.DAL
{
    public class ContactRepository : IContactRepository
    {
        /*
        Use constructor Injection to inject all required dependencies.
        */
        private readonly ContactDataContext db;
        public ContactRepository(ContactDataContext _db)
        {
            this.db = _db;
        }

        /*
       * This method should be used to save a new contact.
       */
        public Contact AddContact(Contact contact)
        {
            
                db.Contacts.Add(contact);
                db.SaveChanges();
                return contact; 
        }

        /*
       * This method should be used to delete an existing contact.
       */
        public bool DeleteContact(int id)
        {
           
                Contact contact = db.Contacts.Find(id);
                if(contact == null)
                {
                    
                    return false;
                }
                else
                {
                    db.Contacts.Remove(contact);
                    db.SaveChanges(true);
                    return true;
                }

        }

        /*
       * This method should be used to get a contact base on specified contactId.
       */
        public Contact GetContactById(int id)
        {

            Contact contact = db.Contacts.Find(id);
            return contact;
            
            
        }

        /*
       * This method should be used to retreive all contacts.
       */
        public List<Contact> GetContacts()
        {

            return db.Contacts.ToList();
            
        }

        /*
       * This method should be used to update an existing contact based on specified contactId.
       */
        public bool UpdateContact(int id, Contact contact)
        {
            
                Contact contact1 = db.Contacts.Find(id);
                if (contact1 != null)
                {
                    //contact1 = contact;
                    contact1.City = contact.City;
                    contact1.FirstName = contact.FirstName;
                    contact1.LastName = contact.LastName;
                    contact1.Email = contact.Email;
                    contact1.Phone = contact.Phone;
                    this.db.Contacts.Update(contact1);
                    this.db.SaveChanges();
                    return true;
                }
                else
                {
                    return false;
                    
                }

           
        }
    }
}
