﻿using cgi_tollgate_assignment_contact_api.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace cgi_tollgate_assignment_contact_api.DAL
{
    public class ContactDataContext:DbContext
    {
        public ContactDataContext(DbContextOptions options) : base(options)
        {
            Database.EnsureCreated();
        }

        // Define Contacts entity of type DbSet<Contact>
        public DbSet<Contact> Contacts { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Contact>(entity =>
            {
                entity.ToTable("Contact");
                entity.HasKey(s => s.ContactId);
                entity.Property(s => s.FirstName).IsRequired().HasMaxLength(50);
                entity.Property(s => s.LastName).HasMaxLength(50).IsRequired();
                entity.Property(s => s.Email).IsRequired().HasMaxLength(200);
                entity.Property(s => s.Phone).IsRequired().HasMaxLength(20);
                entity.Property(s => s.City).IsRequired().HasMaxLength(50);
            });

        }
    }
}
