﻿using cgi_tollgate_assignment_contact_api.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace cgi_tollgate_assignment_contact_api.DAL
{
    public interface IContactRepository
    {
        Contact AddContact(Contact contact);
        Contact GetContactById(int id);
        List<Contact> GetContacts();
        bool UpdateContact(int id, Contact contact);
        bool DeleteContact(int id);
    }
}
