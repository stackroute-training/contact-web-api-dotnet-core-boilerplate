﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using cgi_tollgate_assignment_contact_api.Exceptions;
using cgi_tollgate_assignment_contact_api.Models;
using cgi_tollgate_assignment_contact_api.Services;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace cgi_tollgate_assignment_contact_api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ContactController : ControllerBase
    {
        /*
      * ContactService should  be injected through constructor injection. Please note that we should not create service
      * object using the new keyword
      */
        private readonly IContactService service;
        public ContactController(IContactService _service)
        {
            this.service = _service;
        }

        /*
         * Define a handler method which will get us the all contacts from database table.
         * 
         * This handler method should return any one of the status messages basis on
         * different situations: 1. 200(OK) - If the contact found successfully. 
         * 2. 500(Internal Server Error) - If there is an error while retreiving data from database table.
         * This handler method should map to the URL "/api/contact" using HTTP GET method
         */
        // GET: api/<ContactController>

        [HttpGet]
        public IActionResult Get()
        {
            try
            {
                List<Contact> contacts = service.GetContacts();
                if(contacts == null)
                {
                    return StatusCode(statusCode: 500);
                }
                else
                {
                    return StatusCode(statusCode: 200,contacts);
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        /*
         * Define a handler method which will get us the contact by a contactId.
         * 
         * This handler method should return any one of the status messages basis on
         * different situations: 1. 200(OK) - If the contact found successfully. 
         * 2. 404(NOT FOUND) - If the contact with specified contactId is not found.
         * This handler method should map to the URL "/api/contact/{userId}" using HTTP GET method
         */
        // GET api/<ContactController>/5
        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            try
            {
                Contact contact = service.GetContactById(id);
                return StatusCode(statusCode: 200,contact);
            }
            catch (ContactNotFoundException ex )
            {
                return NotFound(ex.Message);
            }
        }

        /*
         * Define a handler method which will create a contact by reading the
         * Serialized contact object from request body and save the contact in
         * contacts table in database.
         * This handler method should return any one of the status messages
         * basis on different situations: 1. 201(CREATED - In case of successful
         * creation of the contact 2. 500(Internal Server Error) - In case of and error while creating contact
         * 
         *  * This handler method should map to the URL "/api/contact" using HTTP POST method
         */

        // POST api/<ContactController>
        [HttpPost]
        public IActionResult Post([FromBody] Contact contact)
        {
            try
            {
                Contact contacts = service.AddContact(contact);
                if (contacts == null)
                {
                    return StatusCode(statusCode: 500);
                }
                else
                {
                    return StatusCode(statusCode: 201,contacts);
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        /*
         * Define a handler method which will update a specific contact by reading the
         * Serialized object from request body and save the updated contact details in
         * a contacts table in database handle ContactNotFoundException as well. 
         * This handler method should return any one of the status messages basis on different situations:
         * 1. 200(OK) - If the contact updated successfully. 
         * 2. 404(NOT FOUND) - If the contact with specified contactId is not found.
         * This handler method should map to the URL "/api/contact/{id}" using HTTP PUT
         * method.
         */
        // PUT api/<ContactController>/5
        [HttpPut("{id}")]
        public IActionResult Put(int id, [FromBody] Contact contact)
        {
            try
            {
                bool contacts = service.UpdateContact(id, contact);
                
                return StatusCode(statusCode: 200, contacts); 
                
                
            }
            catch (ContactNotFoundException ex)
            {

                return NotFound(ex.Message);
            }
        }

        /* Define a handler method which will delete a contact from a database.
        * 
        * This handler method should return any one of the status messages basis on
        * different situations: 1. 200(OK) - If the contact deleted successfully from
        * database. 2. 404(NOT FOUND) - If the contact with specified contactId is
        * not found. 
        * 
        * This handler method should map to the URL "/api/contact/{id}" using HTTP Delete
        * method" where "id" should be replaced by a valid contactId.
        */
        // DELETE api/<ContactController>/5
        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            try
            {
                bool contacts = service.DeleteContact(id);
                
                return StatusCode(statusCode: 200,contacts);
         
            }
            catch (ContactNotFoundException ex)
            {
                return NotFound(ex.Message);
                
            }
        }
    }
}
